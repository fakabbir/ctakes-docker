# ctakes docker image

`docker run -d -p 8087:8080 -p 3307:3306 -e MYSQL_PASS="pass" ctakes`

CTAKES
===================
ctakes is a library maintained by apache community that provided many functionalities that can
be used to solve problems in medical/pharma industry. One of the usefull model is NER annotator.
ctakes can be used be annotate a sentance/article with terms sutable with them such as drug,
 deease, instrument etc.


ctakes NER is trained on medical docuements and preptrained model is also available. 
We have used ctakes java librarries along with medical term index/id database. The database
can be loaded via scripts present in sno_rc_16ab_db folder. Since the script takes too long
we have dumped into mysql database and took backup that can be restored in seconds.


The java program is wrapped in a rest api. The api base is exactly the same as over the 
open source repository: https://github.com/GoTeamEpsilon/ctakes-rest-service


The build process is also mentioned. The dependencies are huge, so we have build a war once
and are using the same war for any operation. In case building it form scratch is required.
The steps are mentioned in the repository mentioned above.


Right now we have dockerized the war file along with database in the same container.
The services are running using supervisord.


If you are not using docker you can use this script. This script is complete in itself but need manual step by step manual
entry to make sure it works well.

```
#! /bin/bash
# INSTALLATION SCRIPT FOR CTAKES
apt-get update
apt-get install -y default-jre default-jdk wget mysql-server mysql-client libmysqlclient-dev --no-install-recommends
apt-get clean	
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

export JAVA_HOME="/usr/lib/jvm/default-java"

# ctakes need high memory of else will throw heap space error
export CATALINA_OPTS="-Xms5120M -Xmx5120M"

mkdir /opt/tomcat
tar -xf apache-tomcat-8.5.34.tar.gz --directory /opt/tomcat
cp ctakes-web-rest.war /opt/tomcat/apache-tomcat-8.5.34/webapps/

# Write the backup.sql in a database "umls" with user "root" and password "pass"
mysql -u root < reset-user.sql
pkill mysqld
service mysql start

mysql -u root -ppass < CREATE DATABASE umls;
mysql -u root -ppass umls < backup.sql

/opt/tomcat/apache-tomcat-8.5.34/bin/catalina.sh run

```