echo "Starting SQL Server"
service mysql restart
sleep 20
echo "Setting Username and Password"
mysql -u root < reset-user.sql
echo "Restarting Server - SQL"
pkill mysqld
sleep 20
echo ". . . ."
service mysql start
sleep 20
echo "Loading the database ..."
mysql -u root -ppass < create-db.sql
mysql -u root -ppass umls < backup.sql
sleep 20

echo "Running Mysql and Tomcat Server at port 3306 and 8080....."
supervisord -n
